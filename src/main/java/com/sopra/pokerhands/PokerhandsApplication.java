package com.sopra.pokerhands;

import com.sopra.pokerhands.entities.Hand;
import com.sopra.pokerhands.service.HandService;
import com.sopra.pokerhands.service.InputService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

@SpringBootApplication
public class PokerhandsApplication implements CommandLineRunner {

    Logger logger = LoggerFactory.getLogger(PokerhandsApplication.class);

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private InputService inputService;

    @Autowired
    private HandService handService;

    public static void main(String[] args) {
        SpringApplication.run(PokerhandsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        
        Resource resource = resourceLoader.getResource("classpath:poker.txt");
        File file = resource.getFile();
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        logger.info("############## RESULTS ##############");

        while (true) {
            String line = bufferedReader.readLine();
            if (line == null) {
                break;
            }

            if (inputService.validateInput(line)) {
                String[] hands = inputService.manageInput(line);

                Hand blackHand = new Hand(hands[0]);
                Hand whiteHand = new Hand(hands[1]);

                handService.findWinner(blackHand, whiteHand);

            } else {
                logger.info("Input line error - skipping line");
            }
        }
        logger.info("############## FINISHED ##############");
    }
}