package com.sopra.pokerhands.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Hand {
    private List<Card> cards;
    private Ranking ranking;
    private List<Card> cardsCompare;

    public Hand(String handString){
        cards = new ArrayList<>();
        cardsCompare = new ArrayList<>();

        for (String cardString : handString.split(" ")){
            cards.add(new Card(
                    Value.getFromSymbol(cardString.charAt(0))
                    , cardString.charAt(1))
            );
        }

        calculateRanking();
    }

    public Ranking getRanking() {
        return ranking;
    }

    public void setRanking(Ranking ranking) {
        this.ranking = ranking;
    }

    public List<Card> getCardsCompare() {
        return cardsCompare;
    }

    public void setCardsCompare(List<Card> cardsCompare) {
        this.cardsCompare = cardsCompare;
    }

    public List<Card> getCards() {

        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public void calculateRanking() {

        Collections.sort(cards);


        boolean cardsSameColour = true;
        boolean cardsInSequency = true;

        boolean straightFromAce = false;

        for (int i = 1; i < cards.size(); i++) {
            if (cards.get(i).getSuit() != cards.get(0).getSuit()) {
                cardsSameColour = false;
            }
            if (cards.get(i - 1).getValue().getVal() != (cards.get(i).getValue().getVal()-1)) {
                cardsInSequency = false;
            }
            if (!cardsSameColour && !cardsInSequency) {
                break;
            }
        }

        String sequencyAce ="";
        for (Card card : cards) {
            sequencyAce = sequencyAce + card.getValue().getSymbol();
        }

        if (sequencyAce.equals("2345A")) {
            straightFromAce = true;
        }

        if (cardsSameColour && cardsInSequency) {
            cardsCompare.add(cards.get(4));
            this.ranking = Ranking.STRAIGHT_FLUSH;
        } else if (cardsSameColour) {
            cardsCompare.add(cards.get(4));
            this.ranking = Ranking.FLUSH;
        } else if (cardsInSequency ) {
            cardsCompare.add(cards.get(4));
            this.ranking = Ranking.STRAIGHT;
        } else if (straightFromAce) {
            cardsCompare.add(cards.get(3));
            this.ranking = Ranking.LOW_STRAIGHT;
        }

        boolean fourInARow = true;


        for (int i = 1; i < cards.size(); i++) {

            if (cards.get(i - 1).getValue()
                    .compareTo(cards.get(i).getValue()) != 0) {

                for (int j = 2; j < cards.size(); j++) {
                    if (cards.get(j - 1).getValue()
                            .compareTo(cards.get(j).getValue()) != 0) {
                        fourInARow = false;
                        break;
                    }
                }

            }
        }

        boolean treeInARow = true;

        for (int i = 1; i < 3; i++) {
            if (cards.get(i - 1).getValue()
                    .compareTo(cards.get(i).getValue()) != 0) {
                for (int j = 3; j <= 4; j++) {
                    if (cards.get(j - 1).getValue()
                            .compareTo(cards.get(j).getValue()) != 0) {
                        treeInARow = false;
                        break;
                    }
                }
            }
        }

        boolean pair = true;

        if (treeInARow) {
            if (cards.get(0).getValue().compareTo(cards.get(1).getValue())
                    == 0 &&
                    cards.get(1).getValue().compareTo(cards.get(2).getValue()) == 0) {
                if (cards.get(3).getValue().compareTo(cards.get(4).getValue())
                        != 0) {
                    pair = false;
                }
            } else if (cards.get(0).getValue().compareTo(cards.get(1).getValue())
                    != 0) {
                pair = false;
            }

        }

// in case of FourInARow,FullHouse and ThreeInARow, the third card (position 2) is always the one I need
        if (fourInARow) {
            cardsCompare.add(cards.get(2));
            this.ranking = Ranking.FOUR_OF_A_KIND;
        } else if (treeInARow && pair) {
            cardsCompare.add(cards.get(2));
            this.ranking = Ranking.FULL_HOUSE;
        }else if (treeInARow) {
            cardsCompare.add(cards.get(2));
            this.ranking = Ranking.THREE_OF_A_KIND;
        }

        int pairsCount = 0;

        for (int i = 1; i < cards.size(); i++) {

            if (cards.get(i - 1).getValue()
                    .compareTo(cards.get(i).getValue()) == 0) {
                cardsCompare.add(cards.get(i));
                pairsCount++;
                if (pairsCount == 2 && !treeInARow) {
                    addRestOfTheCards(cards, cardsCompare);
                    this.ranking = Ranking.TWO_PAIRS;
                }
            }
        }
        if (pairsCount == 1) {
            addRestOfTheCards(cards, cardsCompare);
            this.ranking = Ranking.PAIR;
        }

        if(!straightFromAce && !cardsSameColour && !cardsInSequency && !fourInARow && !treeInARow && pairsCount == 0){
            cardsCompare = cards;
            Collections.reverse(cardsCompare);
            this.ranking = Ranking.NO_HAND;
        }
    }


    private void addRestOfTheCards(List<Card> cards, List<Card> cardsCompare){
        cards.removeAll(cardsCompare);
        for (Card c : cards){
            cardsCompare.add(c);
        }
    }


    @Override
    public String toString() {
        return super.toString();
    }
}
