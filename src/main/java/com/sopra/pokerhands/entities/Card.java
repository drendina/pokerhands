package com.sopra.pokerhands.entities;

public class Card implements Comparable<Card> {

    private Value value;
    private Character suit;


    public Card(Value value, Character suit){
        this.value = value;
        this.suit = suit;

    }

    public Character getSuit() {
        return suit;
    }

    public void setSuit(Character suit) {
        this.suit = suit;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Card details: " + value + " - " + suit;
    }

    @Override
    public int compareTo(Card card) {
        //if (this.getValue().getVal() == (card.getValue().getVal()-1))
        //    return 1;
        if (this.getValue().getVal() < card.getValue().getVal())
            return -1;
        else if (this.getValue().getVal() > card.getValue().getVal())
            return 1;
        else
            return 0;
        }
}
