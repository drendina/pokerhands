package com.sopra.pokerhands.entities;

public enum Value {
    TWO('2',2,"two"),
    THREE('3',3,"three"),
    FOUR('4',4,"four"),
    FIVE('5',5,"five"),
    SIX('6',6,"six"),
    SEVEN('7',7,"seven"),
    EIGHT('8',8,"eight"),
    NINE('9',9,"nine"),
    TEN('T',10,"ten"),
    JACK('J',11,"jack"),
    QUEEN('Q',12,"queen"),
    KING('K',13,"king"),
    ACE('A',14,"ace");

    Character symbol;
    int val;
    String valueToPrint;

    Value(Character symbol, int val, String print){
        this.symbol=symbol;
        this.val = val;
        this.valueToPrint = print;
    }

    public Character getSymbol() {
        return symbol;
    }

    public void setSymbol(Character symbol) {
        this.symbol = symbol;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public String getValueToPrint() {
        return valueToPrint;
    }

    public void setValueToPrint(String valueToPrint) {
        this.valueToPrint = valueToPrint;
    }

    public static Value getFromSymbol(Character symbol)
    {
        for (Value v : Value.values()){
            if (v.symbol == symbol)
                return v;
        }
        return null;
    }
}
