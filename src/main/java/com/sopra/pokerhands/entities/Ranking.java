package com.sopra.pokerhands.entities;

public enum Ranking {

    NO_HAND(1, "high card:"),
    PAIR(2, "pair"),
    TWO_PAIRS(3, "two pairs"),
    THREE_OF_A_KIND(4, "three of a kind"),
    LOW_STRAIGHT(5, "straight"),
    STRAIGHT(6, "straight"),
    FLUSH(7, "flush"),
    FULL_HOUSE(8, "full house"),
    FOUR_OF_A_KIND(9, "four of a kind"),
    STRAIGHT_FLUSH(10, "straight flush");

    int position;
    String out;

    Ranking(int position, String out){
        this.position = position;
        this.out = out;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getOut() {
        return out;
    }

    public void setOut(String out) {
        this.out = out;
    }

}
