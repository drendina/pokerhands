package com.sopra.pokerhands.service;

public interface InputService {
    boolean validateInput(String input);
    String[] manageInput(String input);
}
