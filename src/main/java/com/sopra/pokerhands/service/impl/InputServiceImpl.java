package com.sopra.pokerhands.service.impl;

import com.sopra.pokerhands.service.InputService;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class InputServiceImpl implements InputService {

    @Override
    public boolean validateInput(String input) {
        String regex = "^Black:{1}(( [2-9TJQKA]{1}[CDHS]{1}){5})([ ]{2})White:{1}(( [2-9TJQKA]{1}[CDHS]{1}){5})$";
        Pattern pattern = Pattern.compile(regex);
        if (input == null)
            return false;

        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    @Override
    public String[] manageInput(String input) {
        return input.replace("Black: ", "")
                .replace("White: ", "")
                .replace("  ", "-")
                .split("-");
    }


}
