package com.sopra.pokerhands.service.impl;

import com.sopra.pokerhands.entities.Hand;
import com.sopra.pokerhands.entities.Ranking;
import com.sopra.pokerhands.service.HandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;



@Service
public class HandServiceImpl implements HandService {

    Logger logger = LoggerFactory.getLogger(HandService.class);
    @Override
    public void findWinner(Hand blackHand, Hand whiteHand) {

        if(whiteHand.getRanking().getPosition() > blackHand.getRanking().getPosition()){
            printWinner("White", whiteHand);
        }else if(whiteHand.getRanking().getPosition() < blackHand.getRanking().getPosition() ){
            printWinner("Black", blackHand);
        }else{
            manageTie(blackHand, whiteHand);
        }
    }

    private void printWinner(String winner, Hand hand){
        Ranking rank = hand.getRanking();
        String outString = winner + " wins with " + rank.getOut() + " ";
        switch(rank){
            case STRAIGHT_FLUSH:
                logger.info(outString);
                break;
            case FOUR_OF_A_KIND: case STRAIGHT: case LOW_STRAIGHT: case FLUSH: case THREE_OF_A_KIND: case PAIR:  case NO_HAND:
                logger.info(outString + "( high card: " + hand.getCardsCompare().get(0).getValue().getValueToPrint() + " )");
                break;
            case FULL_HOUSE:
                outString = outString.concat(hand.getCardsCompare().get(0).getValue().getSymbol() + " over ");
                hand.getCards().removeAll(hand.getCardsCompare());
                logger.info(outString + hand.getCards().get(0).getValue().getSymbol());
                break;
            case TWO_PAIRS:
                logger.info(outString + hand.getCardsCompare().get(0).getValue().getSymbol() + " over " + hand.getCardsCompare().get(1).getValue().getSymbol());
                break;
        }
    }

    private void manageTie(Hand blackHand, Hand whiteHand){
        Ranking rank = blackHand.getRanking();
        boolean draw = true;
        switch (rank){
            case STRAIGHT_FLUSH:
                break;
            default:
                for (int i = 0; i < blackHand.getCardsCompare().size(); i++) {
                    if(blackHand.getCardsCompare().get(i).getValue().getVal() >
                            whiteHand.getCardsCompare().get(i).getValue().getVal()){
                        draw = false;
                        printWinner("Black", blackHand);
                        break;
                    }else if (whiteHand.getCardsCompare().get(i).getValue().getVal() >
                            blackHand.getCardsCompare().get(i).getValue().getVal()) {
                        draw = false;
                        printWinner("White", whiteHand);
                        break;
                    }
                    else {
                        whiteHand.getCardsCompare().remove(i);
                        blackHand.getCardsCompare().remove(i);
                    }
                }
                if(draw) {
                    logger.info("The result is a draw");
                }
        }
    }
}