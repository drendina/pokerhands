package com.sopra.pokerhands.service;

import com.sopra.pokerhands.entities.Hand;

public interface HandService {
    void findWinner(Hand whiteHand, Hand blackHand);
}
