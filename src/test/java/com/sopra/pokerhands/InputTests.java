package com.sopra.pokerhands;

import com.sopra.pokerhands.service.impl.InputServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@SpringBootTest
public class InputTests {

    @Test
    public void testGoodInput() {
        InputServiceImpl inputServiceImpl = new InputServiceImpl();
        String goodInput = "Black: 2H 3D 5S 9C KD  White: 2C 3H 4S 8C AH";
        assertTrue(inputServiceImpl.validateInput(goodInput));
    }

    @Test
     void testBadInputMissingCard() {
        InputServiceImpl inputServiceImpl = new InputServiceImpl();
        String missingCard = "Black: 2H 3D 5S 9C KD  White: 2C 3H 4S 8C";
        assertFalse(inputServiceImpl.validateInput(missingCard));
    }

    @Test
    public void testBadInputInexistentCard() {
        InputServiceImpl inputServiceImpl = new InputServiceImpl();
        String inexistentCard = "Black: 2H 3D 5S 9C KD  White: 2C 3H 4S 8W";
        assertFalse(inputServiceImpl.validateInput(inexistentCard));

    }
}
