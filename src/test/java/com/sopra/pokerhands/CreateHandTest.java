package com.sopra.pokerhands;

import com.sopra.pokerhands.entities.Hand;
import com.sopra.pokerhands.entities.Ranking;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class CreateHandTest {

    @Test
    void testNoHand() {
        Hand hand = new Hand("AC TD 5D 3D 8S");
        assertEquals(null, hand.getRanking(), Ranking.NO_HAND);

    }

    @Test
    void testPair(){
        Hand hand =  new Hand("2C 3H 3C KS TS");
        assertEquals(null, hand.getRanking(), Ranking.PAIR );
    }

    @Test
    void testTwoPairs(){
        Hand hand =  new Hand("TC 3H 3C KS TS");
        assertEquals(null, hand.getRanking(), Ranking.TWO_PAIRS );
    }
    
    @Test
    void testThreeOfAKind(){
        Hand hand =  new Hand("3C 5S 3H 8S 3S");
        assertEquals(null, hand.getRanking(), Ranking.THREE_OF_A_KIND );
    }
    
    @Test
    void testLowStraight(){
        Hand hand =  new Hand("5C 3S 4S AD 2D");
        assertEquals(null, hand.getRanking(), Ranking.LOW_STRAIGHT );
    }

    @Test
    void testStraight(){
        Hand hand =  new Hand("5C 3S 4S 6D 2D");
        assertEquals(null, hand.getRanking(), Ranking.STRAIGHT );
    }
    
    @Test
    void testFlush(){
        Hand hand =  new Hand("5D 7D 9D AD QD");
        assertEquals(null, hand.getRanking(), Ranking.FLUSH );
    }

    @Test
    void testFullHouse(){
        Hand hand =  new Hand("5C 8C 5D 8H 8D");
        assertEquals(null, hand.getRanking(), Ranking.FULL_HOUSE );
    }

    @Test
    void testFourOfAKind(){
        Hand hand =  new Hand("AH AD AS AC TC");
        assertEquals(null, hand.getRanking(), Ranking.FOUR_OF_A_KIND );
    }

    @Test
    void testStraightFlush(){
        Hand hand =  new Hand("JC QC KC AC TC");
        assertEquals(null, hand.getRanking(), Ranking.STRAIGHT_FLUSH );
    }
}
